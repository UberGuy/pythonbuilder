### Python Builder

This repository contains a mix of shell scripts and Dockerfiles which automate compiling a largely stand-alone Python environment in a location of your choosing. The end result is a tarball that can be expanded with absolute paths to drop the Python installation in the chosen location.

The builder has been tested with the following Python versions:

- 2.7.18
- 3.6.15
- 3.8.12
- 3.10.0

It has been tested with the following base operating system images

- Ubuntu 16.04
- Ubuntu 18.04
- Ubuntu 20.04
- CentOS 7.4
- Centos 8.4
- Rocky Linux 8.4
- Amazon Linux 2

### Running the builder.

From the root of the repo, run the following command.

`./do_build [PYTHON_VERSION] [PYTHON_INSTALL_DIR]`

`PYTHON_VERSION` must be a semver-style string of three dot-delimited integers, like `3.6.15` The scripts to not support more complex symver strings.

If you provide no arguments or environment variables, the script will build Python 3.8.12 for Ubuntu 20.04, located in `/opt/python`.

You can override the OS it will build for by setting the environment variable `base_os`, which should be a string in the form `os_name:os_version`. The value of `os_name` should map to the suffix on a `Dockerfile-*`filename in the repo; this is how the Dockerfile to build from is chosen.

Currently,the following values are valid:

- `amazonlinux`
- `centos`
- `rockylinux`
- `ubuntu`

The `os_version` part of `base_os` is optional. If omitted, a default value from the Dockerfile will be used. If given, it must map to an available tag for the `FROM` image name in the Dockerfile matched by `os_name`.

The `do_build` script has only been tested on Ubuntu with `bash` 5.0. I tried to reserve more complicated logic for scripts that are run inside the Docker containers, leaving this script pretty basic, so it will _probably_ work on MacOS, but I didn't test it.

### Output artifacts

The Python installation bundle is written to a directory that is bind mounted to the `/tarballs` directory in the repo. It's recommended that you relocate these after each build, _especially_ if you plan to build multiple installations, since all the files here are sourced into the Docker environment on start of later builds. The process is fast, but if you have more than a few installs here, it starts to get noticable.

The scripts download sources for Python and the bundled dependencies. These source tarballs are downloaded to a directory that is bind mounted to the `/source_downloads` directory in the repo. This is so that they don't need to be downloaded again in case a build fails, is aborted, etc. If you don't need these files after a build is complete feel free to delete them.

You are also left with a Docker image for each OS+version used to build Python, plus the base OS image. For example, after building for Ubuntu 20.04, my system was left with these images.

```
REPOSITORY            TAG            SIZE
python-builder        ubuntu-20.04   349MB
ubuntu                20.04          72.8MB
```

Like with the source downloads, these are left in place so they don't need to be re-downloaded / rebuilt every time you want to build a Python bundle. However, you likely want to clean these up once you no longer need them.

### What does "stand-alone" mean?

A Python installation that actually enables all of its standard library packages requires either the installation or the compilation of quite a few external libraries. This tends to tie your Python install to those shared library versions, and while not super common, your Python (or at least its access some packages) can end up broken if you upgrade those libraries on an existing system. You can end up in a form of dependency hell, where some tools require
older or newer versions of system-wide libraries, like OpenSSL, than your Python install(s) want.

One way to avoid this is to have your Python installation "bundled" with its own copies of the shared libraries it needs.* This is what "stand-alone" means in this context - the installation doesn't depend on your OS having a globally shared version of these libraries installed.

Picking what libraries to include in such a "bundle" requires a bit of selection. Some very fundamental globally-shared libraries are pretty safe to rely on not introducing breaking changes unless you update the whole OS. Some, like the C++ runtime, can change, but are a lot of work to "bundle". I've picked what I considered the "low hanging fruit" of things to bundle with the Python install - libraries that having in the bundle will most frequently (in my experience) protect it from dependency hell or breakage due to unexpected library upgrades.

- bzip2
- libffi
- libuuid
- ncurses (for readline)
- open_ssl
- readline
- sqlite
- xz
- zlib

\* This is far from the only way to achieve such isolation. You could use static linking of the libraries, run Python from a Docker container, or install it using something like an [AppImage](https://appimage.org/) bundle. (In fact, this repo is basically a very limited version of how the most commonly used Python AppImages are built.)

### Future Improvements

- The `do_build` script needs a real usage output.
- The `do_build` script could use better argument parsing, using optional args instead of a mix of positional args and environment variables.
- Don't compile `libuuid` unless Python version 3.7+ is being built.
- Command-line options to automatically clean up the Docker images downloaded and built would be nice for people who don't plan to run builds more than once in a blue moon.
- I've been a bit lazy about _what_ is built and/or included in the resulting tarball for each of these libraries. Many of them come with command-line tools and (sometimes extensive) help files that really don't need to be included along with the Python  environment. It would be worth looking into the `configure` switches that omit building some of these things, or removing them from the resulting build before creating the output tarball.
